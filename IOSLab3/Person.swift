//
//  Person.swift
//  IOSLab3
//
//  Created by iosdev on 29.3.2020.
//  Copyright © 2020 Ville Liekari. All rights reserved.
//

import Foundation

class Singleton: NSObject {
    static let sharedInstance = Singleton()
    var bmiHistory = [Person]()
}

class Person {
    let name: String
    let age: Int
    let profession: [String]
    let height: Double
    let weight: Double
    
    init?(name: String, age: Int, profession: [String], height: Double, weight: Double){
        guard !name.isEmpty else {
            return nil
        }
        guard age >= 5 && age <= 120 else {
            return nil
        }
        guard profession.count >= 3 else {
            return nil
        }
        guard weight >= 30 && weight <= 300 else {
            return nil
        }
        guard height >= 90 && height <= 250 else {
            return nil
        }
        self.name = name
        self.age = age
        self.profession = profession
        self.height = height
        self.weight = weight
    }
    
    func getPerson() {
        print("Person: \(name)\nAge: \(age)\nProfession: \(profession[0]) at \(profession[1]), line \(profession[2])\nHeight: \(height)\nHeight: \(weight)")
    }
    
    func getBMI() -> Double {
        let heightMeters = height/100
        let bmi = weight / (heightMeters * heightMeters)
        let roundedBMI = Double(round(10*bmi)/10)
        return roundedBMI
    }
}
