//
//  BMITableViewController.swift
//  IOSLab3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Ville Liekari. All rights reserved.
//

import UIKit

class BMITableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Singleton.sharedInstance.bmiHistory.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "BMITableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BMITableViewCell else {
            fatalError("tableview cell error bmitablecellview")
        }

        let history = Singleton.sharedInstance.bmiHistory[indexPath.row]
        cell.nameHistoryLabel.text = "Name: \(history.name)"
        cell.bmiHistoryLabel.text = "Height: \(Double(round(history.height*10)/10))cm, Weight: \(Double(round(history.weight*10)/10))kg, BMI: \(history.getBMI())"

        return cell
    }

    //MARK: Actions
    func addToList(name: String, age: Int, profession: [String], height: Double, weight: Double) -> Double {
        guard let person = Person(name: name, age: age, profession: profession, height: height, weight: weight) else { fatalError("New person failure") }
        Singleton.sharedInstance.bmiHistory += [person]
        self.tableView.reloadData()
        let bmi = person.getBMI()
        return bmi
        
        
    }
}
