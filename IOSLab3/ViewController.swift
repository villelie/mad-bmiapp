//
//  ViewController.swift
//  IOSLab4
//
//  Created by iosdev on 29.3.2020.
//  Copyright © 2020 Ville Liekari. All rights reserved.
//

import UIKit
import os.log

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //MARK: Properties
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var heightPicker: UIPickerView!
    @IBOutlet weak var weightPicker: UIPickerView!
    @IBOutlet weak var bmiLabel: UILabel!
    
    
    var heightData = (90...250).map { "\($0) cm" }
    var weightData = (30...300).map { "\($0) kg" }
    var selectedName: String = ""
    var selectedHeight: Double = 0.0
    var selectedWeight: Double = 0.0
    
    var bmiItem: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameField.delegate = self
        heightPicker.delegate = self
        heightPicker.dataSource = self
        weightPicker.delegate = self
        weightPicker.dataSource = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) { //hide kb.
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == nameField {
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
            
            
        } else {
            return false
        }
    }
    
    
    //MARK: PickerViewStuff
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return heightData.count
        } else {
            return weightData.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return heightData[row]
        } else {
            return weightData[row]
        }
    }
    
    //MARK: Button to do it all
    @IBAction func bmiButton(_ sender: UIButton) {
        if (nameField.text == "") {
            let alert = UIAlertController(title: "Empty name is not allowed!", message: "Enter your name.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        } else {
            selectedName = nameField.text ?? "unknown"
            selectedHeight = Double(heightData[heightPicker.selectedRow(inComponent: 0)].filter("0123456789".contains)) ?? 90
            selectedWeight = Double(weightData[weightPicker.selectedRow(inComponent: 0)].filter("0123456789".contains)) ?? 30
            let result = BMITableViewController().addToList(name: selectedName, age: 5, profession: ["0", "1", "2"], height: selectedHeight, weight: selectedWeight)
            bmiLabel.text = "BMI: \(result)"
        }
    }
    @IBAction func backToView (segue: UIStoryboardSegue) {
        
    }
}
