//
//  BMITableViewCell.swift
//  IOSLab3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Ville Liekari. All rights reserved.
//

import UIKit

class BMITableViewCell: UITableViewCell {

    @IBOutlet weak var nameHistoryLabel: UILabel!
    @IBOutlet weak var bmiHistoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
