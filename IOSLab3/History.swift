//
//  History.swift
//  IOSLab3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Ville Liekari. All rights reserved.
//

import Foundation

class Singleton: NSObject {
    static let sharedInstance = Singleton()
    var bmiHistory = [Person]()
}
