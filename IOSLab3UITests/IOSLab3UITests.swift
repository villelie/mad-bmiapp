//
//  IOSLab3UITests.swift
//  IOSLab3UITests
//
//  Created by iosdev on 29.3.2020.
//  Copyright © 2020 Ville Liekari. All rights reserved.
//

import XCTest

class IOSLab3UITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
    
    //MARK: Tests
    func testBMI() {
        let app = XCUIApplication()
        app.launch()
        let namefield = app.textFields["Your name?"]
        namefield.tap()
        namefield.typeText("testname")
        app.staticTexts["BMI"].tap() //hide keyboard.
        app/*@START_MENU_TOKEN@*/.pickerWheels["90 cm"]/*[[".pickers.pickerWheels[\"90 cm\"]",".pickerWheels[\"90 cm\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        app/*@START_MENU_TOKEN@*/.pickerWheels["30 kg"]/*[[".pickers.pickerWheels[\"30 kg\"]",".pickerWheels[\"30 kg\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        app.buttons["Calculate BMI"].tap()
        app.buttons["History"].tap()
        app.tables.buttons["Back"].tap()
    }
}
